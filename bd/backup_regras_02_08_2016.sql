
INSERT INTO regras VALUES (24, 'select 
   ''ZUPRIO'' AS regra, 
    3 AS criticidade, 
    msg AS mensagem, 
    poi AS poi 
from 
    Mensagem AS msg unidirectional,
    PoI AS poi 
where
  ((description.toUpperCase() like ''%EM ABERTO%'') OR (description.toUpperCase() like ''%EM ANDAMENTO%'')) AND (description.toUpperCase() like ''%ACIDENTE%'') AND categoryID = 16234', 'ZUPRIO', 'ativa');
INSERT INTO regras VALUES (12, 'SELECT 
   ''CET-RIO'' AS regra, 
   3 AS criticidade, 
   msg AS mensagem, 
   poi AS poi 
from 
    Mensagem AS msg unidirectional,
    PoI AS poi 
where  categoryID IN (744,1007,1021,1036)

', 'CET-RIO', 'ativa');
INSERT INTO regras VALUES (11, 'SELECT ''LIGHT'' AS regra, * FROM MensagemWindow WHERE categoryID = 1072', 'LIGHT', 'inativa');
INSERT INTO regras VALUES (14, 'select ''WAZE'' as regra, * from MensagemWindow where (title.toUpperCase() like ''%GRAVE%'' AND categoryID IN (1197,1200,1201) ) OR
        (title.toUpperCase() like ''%NA VIA%'' AND categoryID IN (1197,1200,1201) )  OR
 categoryID IN (16286,16288,16290,16291,16295)
 
       
        ', 'WAZE', 'inativa');
INSERT INTO regras VALUES (18, 'select 
      ''MOOVIT'' AS regra, 
      3 AS criticidade, 
      msg AS mensagem, 
      poi AS poi 
from 
          Mensagem AS msg unidirectional,
          PoI AS poi
where
       (description.toUpperCase() like ''%EMERGENCIA%'' AND categoryID IN (16310,16278) )
       OR (description.toUpperCase() like ''%LOTADA%'' AND categoryID IN (16310,16278) )
       OR (description.toUpperCase() like ''%INCENDIO%'' AND categoryID IN (16310,16278) )
       OR (description.toUpperCase() like ''%ACIDENTE%'' AND categoryID IN (16310,16278) )
       OR (description.toUpperCase() like ''%SEGURANCA%'' AND categoryID IN (16310,16278) )
       OR (description.toUpperCase() like ''%FORA DE SERVICO%'' AND categoryID IN (16310,16278) )
       OR (description.toUpperCase() like ''%FECHADA%'' AND categoryID IN (16310,16278) )
 
 
 
 ', 'MOOVIT', 'ativa');
INSERT INTO regras VALUES (13, 'SELECT 
    ''COMANDO'' AS regra, 
    3 AS criticidade, 
   msg AS mensagem, 
   poi AS poi
FROM 
    Mensagem AS msg unidirectional,
    PoI AS poi 
WHERE 
   categoryID = 16411', 'COMANDO', 'ativa');


SELECT pg_catalog.setval('regras_id_regra_seq', 24, true);

