package br.pucrio.inf.cep;

import br.pucrio.inf.domain.Rule;
import br.pucrio.inf.domain.SituacaoRegra;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class CEPEngineTest {

    private CEPEngine cep;

    @Before
    public void reset() {
        this.cep = CEPEngine.getInstance();
        this.cep.destroyAllRules();
    }

    @Test
    public void createInvalidRule() {
        // Try to add invalid rule
        assertFalse(cep.addRule(new Rule(1, "SELECT * FROM XYZ", "teste", SituacaoRegra.ativa)));
    }

    @Test
    public void createSingleRule() {
        // Assert that there is no deployed rules
        assertEquals(0, cep.getDeployedRules().size());

        assertTrue(cep.addRule(new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa)));

        // Assert that we deployed this rule
        assertEquals(1, cep.getDeployedRules().size());
    }

    @Test
    public void createSameRule() {
        assertTrue(cep.addRule(new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa)));
        assertFalse(cep.addRule(new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa)));

        // Assert that we only have one rule
        assertEquals(1, cep.getDeployedRules().size());
    }

    @Test
    public void revemoRule() {
        // Create a Rule
        assertTrue(cep.addRule(new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa)));

        // Assert that we only have one rule
        assertEquals(1, cep.getDeployedRules().size());

        // Delete a Rule
        assertTrue(cep.removeRule(new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa)));

        // Assert that there is no deployed rules
        assertEquals(0, cep.getDeployedRules().size());
    }

    @Test
    public void removeUnexistingRule() {
        // Try to delete a Rule
        assertTrue(cep.removeRule(new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa)));
    }

    @Test
    public void synchronizeEmptyRules() {
        // Rule Set
        Rule r1 = new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa);
        Rule r2 = new Rule(2, "SELECT * FROM Categoria", "teste", SituacaoRegra.ativa);
        Rule r3 = new Rule(3, "SELECT * FROM Geometria", "teste", SituacaoRegra.ativa);
        Rule r4 = new Rule(4, "SELECT * FROM Mensagem WHERE id = 20", "teste", SituacaoRegra.ativa);

        Set<Rule> ruleSet = new HashSet<>();
        ruleSet.add(r1);
        ruleSet.add(r2);
        ruleSet.add(r3);
        ruleSet.add(r4);

        assertTrue(cep.synchronizeRuleSet(ruleSet));

        // Assert that there is four deployed rules
        assertEquals(4, cep.getDeployedRules().size());
    }

    @Test
    public void synchronizeExistingRuleSet() {
        // Rule Set
        Rule r1 = new Rule(1, "SELECT * FROM Mensagem", "teste", SituacaoRegra.ativa);
        Rule r2 = new Rule(2, "SELECT * FROM Categoria", "teste", SituacaoRegra.ativa);

        Set<Rule> ruleSet = new HashSet<>();
        ruleSet.add(r1);
        ruleSet.add(r2);

        assertTrue(cep.synchronizeRuleSet(ruleSet));

        // New Rule Set
        Rule r3 = new Rule(3, "SELECT * FROM Geometria", "teste", SituacaoRegra.ativa);
        Rule r4 = new Rule(4, "SELECT * FROM Mensagem WHERE id = 20", "teste", SituacaoRegra.ativa);

        ruleSet = new HashSet<>();
        ruleSet.add(r1);
        ruleSet.add(r3);
        ruleSet.add(r4);
        assertTrue(cep.synchronizeRuleSet(ruleSet));

        // Assert that there is three deployed rules
        assertEquals(3, cep.getDeployedRules().size());
    }

}
