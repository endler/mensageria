package br.pucrio.inf;

import br.pucrio.inf.cep.CEPEngine;
import br.pucrio.inf.threads.RulesReaderThread;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class ReaderTest {

    @Before
    public void clear() {
        CEPEngine.getInstance().destroyAllRules();
    }

    @Test
    public void RulesReaderTest() {
        CEPEngine cep = CEPEngine.getInstance();
        assertEquals(0, cep.getDeployedRules().size());
        RulesReaderThread reader = new RulesReaderThread(null);
        reader.run();
        assertEquals(1, cep.getDeployedRules().size());
    }

}
