package br.pucrio.inf.inputstream;

import br.pucrio.inf.database.ConnectionSGBD;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rafael
 */
public class KmlReaderTest {

    @Test
    public void parseKml() {
        this.cleanPOI();
//        this.readPoi("poi/geowazePALANTIR_CLUSTERS.kml");
        this.readPoi("poi/doc.kml");
//        this.readPoi("poi/caminho_espectadores.kml");
//        this.readPoi("poi/acesso_instalacoes.kml");
//        this.readPoi("poi/olimpic_lanes.kml");
//        this.readPoi("poi/outras.kml");
    }

    private void readPoi(String fileSrc) {
        InputStream is;
        try {
            is = new FileInputStream(fileSrc);
            Kml kml = Kml.unmarshal(is);
            Feature feature = kml.getFeature();
            KmlReader kmlReader = new KmlReader();
            kmlReader.parseFeature(feature);
            Assert.assertNotNull(is);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(KmlReaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cleanPOI() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("delete from poi;");
            pps.execute();
        } catch (SQLException ex) {
            System.err.print(ex);
        }
    }

}
