package br.pucrio.inf.inputstream;

import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.security.auth.login.CredentialException;
import junit.framework.TestCase;
import org.junit.Test;

public class GeoPortalAPITest extends TestCase {

    @Test(expected = CredentialException.class)
    public void testInvalid() throws CredentialException, IOException {
        new GeoPortalAPI("3243124321423");
    }

    @Test
    public void testValid() throws CredentialException, IOException {
        new GeoPortalAPI("992d45bf6e092372c38530963f0f8f37");
    }

    @Test
    public void testCategoriesListing() throws CredentialException, IOException {
        Properties prop = new Properties();
        prop.setProperty("chave", "992d45bf6e092372c38530963f0f8f37");

        GeoPortalAPI api = new GeoPortalAPI(prop);
        List<Categoria> categorias = api.listCategories();

        assertTrue(!categorias.isEmpty());

        // Check if categorias containg the following cat:
        Categoria cat = new Categoria("Ocorrências de Trânsito / Colisão", "1", 744);
        boolean found = false;

        for (Categoria listedCategory : categorias) {
            if ((listedCategory.getId() == cat.getId()) && (listedCategory.getDescription().equalsIgnoreCase(cat.getDescription()))) {
                found = true;
            }
        }

        assertTrue(found);
    }

    @Test
    public void testMessageListing() throws CredentialException, IOException {
        Properties prop = new Properties();
        prop.setProperty("chave", "992d45bf6e092372c38530963f0f8f37");

        GeoPortalAPI api = new GeoPortalAPI(prop);
        List<Mensagem> mensagens = api.getMessages(new Categoria("Ocorrências de Trânsito / Colisão", "1", 744));

        assertTrue(!mensagens.isEmpty());

    }
}
