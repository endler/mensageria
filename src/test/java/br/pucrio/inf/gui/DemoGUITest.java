package br.pucrio.inf.gui;

import br.pucrio.inf.cep.CEPEngine;
import br.pucrio.inf.database.ConnectionSGBD;
import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.Rule;
import br.pucrio.inf.domain.SituacaoRegra;
import br.pucrio.inf.inputstream.GeoPortalAPI;
import br.pucrio.inf.inputstream.PerformanceAPI;
import br.pucrio.inf.outputstream.StandardOutput;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.security.auth.login.CredentialException;
import static org.junit.Assert.*;
import org.junit.Test;

public class DemoGUITest {

    @Test
    public void testInsertMessage() throws IOException, CredentialException {
        DemoGUI demoGUI = new DemoGUI();
        demoGUI.setVisible(true);

        PerformanceAPI api = new PerformanceAPI("992d45bf6e092372c38530963f0f8f37");
        List<Mensagem> mensagens = api.getMessages(new Categoria("Ocorrências de Trânsito / Colisão", "1", 744));

        demoGUI.addInputMessages(mensagens);

        assertEquals(mensagens.size(), demoGUI.getInputTableRowCount());
    }

    @Test
    public void testFilterMessage() throws IOException, CredentialException {
        DemoGUI demoGUI = new DemoGUI();
        demoGUI.setVisible(true);

        // Create CEP Rule
        Rule r1 = new Rule(1, "SELECT 'Obra na Via' AS regra, * FROM MensagemWindow WHERE categoryID = 752", "Obra na Via", SituacaoRegra.ativa);
        CEPEngine cep = CEPEngine.getInstance();
        cep.addRule(r1);

        for (int i = 0; i < 5; i++) {
            cep.clear();
            GeoPortalAPI api = new GeoPortalAPI("992d45bf6e092372c38530963f0f8f37");
            List<Categoria> categorias = api.listCategories();

            for (Categoria listedCategory : categorias) {
                List<Mensagem> mensagens = api.getMessages(listedCategory);
                for (Mensagem msg : mensagens) {
                    cep.getCepEngine().getEPRuntime().sendEvent(msg);
                }
            }

            List<Mensagem> mensagens = api.getMessages(new Categoria("Obra na Via", "1", 752));
            demoGUI.addOutputMessages(StandardOutput.getInstance().getMsgOutputQueue());
            assertEquals(mensagens.size(), demoGUI.getOutputTableRowCount());
        }
    }

    @Test
    public void testRuleMessage() throws IOException, CredentialException, SQLException {
        DemoGUI demoGUI = new DemoGUI();
        demoGUI.setVisible(true);
        ConnectionSGBD connection = ConnectionSGBD.getInstance();

        PreparedStatement pps = connection.getConnection().prepareStatement("select * from regras where situacao like 'ativa'");
        ResultSet rs = pps.executeQuery();
        Set<Rule> rules = new HashSet<>();
        while (rs.next()) {
            rules.add(new Rule(rs.getInt(1), rs.getString(2), rs.getString(3), SituacaoRegra.valueOf(rs.getString(4))));
        }
        demoGUI.addOutputMessages(StandardOutput.getInstance().getMsgOutputQueue());
        demoGUI.addInputFilters(rules);

        assertEquals(rules.size(), demoGUI.getFilterTableRowCount());
        System.out.println("parei aki");
    }

}
