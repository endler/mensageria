package br.pucrio.inf;

import br.pucrio.inf.cep.CEPEngine;
import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.MensagemFiltrada;
import br.pucrio.inf.domain.Rule;
import br.pucrio.inf.domain.SituacaoRegra;
import br.pucrio.inf.inputstream.PerformanceAPI;
import br.pucrio.inf.outputstream.StandardOutput;
import br.pucrio.inf.threads.PoIReadersThread;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.security.auth.login.CredentialException;

import org.junit.Before;
import org.junit.Test;

public class CEPUsageTest {

    private CEPEngine cep;
    private StandardOutput output;

    @Before
    public void reset() {
        this.cep = CEPEngine.getInstance();
        this.output = StandardOutput.getInstance();
        this.output.clear();
    }

    @Test
    public void simpleRuleUsage() throws CredentialException, IOException, InterruptedException {
        new PoIReadersThread().run();

        // Rule Set
        Set<Rule> ruleSet = new HashSet<>();
        ruleSet.add(new Rule(1, " SELECT 'GEO' AS regra, msg"
                + "  FROM MensagemWindow AS msg unidirectional,"
                + "       PoI AS poi"
                + " WHERE msg.contained(poi) AND"
                + "      (categoryID=1011 OR categoryID=1012 OR categoryID=1013 OR categoryID=1200)"
                + " LIMIT 1", "BB", SituacaoRegra.ativa));

        cep.synchronizeRuleSet(ruleSet);

        // Let's consume GeoPortal Stream
        PerformanceAPI api = new PerformanceAPI("992d45bf6e092372c38530963f0f8f37");
        List<Categoria> categoryList = api.listCategories();

        for (Categoria categoria : categoryList) {
            List<Mensagem> msgList = api.getMessages(categoria);

            for (Mensagem msg : msgList) {
                cep.getCepEngine().getEPRuntime().sendEvent(msg);
            }
        }
        Thread.sleep(1000);

        System.out.println("tamanho: " + output.getMsgOutputQueue().size());
        for (MensagemFiltrada msg : output.getMsgOutputQueue()) {
            System.out.println(msg.getMsg().getLatitude() + "," + msg.getMsg().getLongitude());
        }

    }

}
