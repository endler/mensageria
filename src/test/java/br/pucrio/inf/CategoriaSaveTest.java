/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf;

import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.inputstream.PerformanceAPI;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.security.auth.login.CredentialException;
import static junit.framework.TestCase.assertTrue;
import org.junit.Test;

/**
 *
 * @author Rafael
 */
public class CategoriaSaveTest {

    @Test
    public void testCategoriesListing() throws CredentialException, IOException {
        Properties prop = new Properties();
        prop.setProperty("chave", "992d45bf6e092372c38530963f0f8f37");

        PerformanceAPI api = new PerformanceAPI(prop);
        List<Categoria> categorias = api.listCategories();

        assertTrue(!categorias.isEmpty());

        // Check if categorias containg the following cat:
        Categoria cat = new Categoria("Ocorrências de Trânsito / Colisão", "1", 744);

        for (Categoria listedCategory : categorias) {
            assertTrue(listedCategory.save());
            List<Mensagem> mensagens = api.getMessages(listedCategory);
            for (Mensagem msg : mensagens) {
                assertTrue(msg.save());
            }
        }

    }
}
