package br.pucrio.inf.database;

import br.pucrio.inf.domain.Configuration;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael
 */
public class ConnectionSGBD {

    protected static Connection connection;
    private static ConnectionSGBD singleton;

    private void connect() {
        if (connection == null) {
            try {
                Properties config = Configuration.getProperties();
                switch (config.getProperty("sgbd")) {
                    case "sqlserver":
                        connection = DriverManager.getConnection(config.getProperty("urlSQLServer") + "databaseName=" + config.getProperty("databaseName") + ";", config.getProperty("userSQLServer"), config.getProperty("pwdSQLServer"));
                        break;
                    case "postgresql":
                        Class.forName("org.postgresql.Driver");
                        System.out.println(config.getProperty("urlPostgres") + config.getProperty("databaseName"));
                        connection = DriverManager.getConnection(config.getProperty("urlPostgres") + config.getProperty("databaseName"), config.getProperty("userPostgres"), config.getProperty("pwdPostgres"));
                        break;
                    case "oracle":
                        connection = DriverManager.getConnection(config.getProperty("urlOracle") + config.getProperty("databaseName"), config.getProperty("userOracle"), config.getProperty("pwdOracle"));
                        break;
                    default:
                        throw new UnsupportedOperationException("Atributo SGBD do arquivo de par�metros (.properties) nao foi atribuido corretamente.");
                }
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(ConnectionSGBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private ConnectionSGBD() {
        connect();
    }

    public static ConnectionSGBD getInstance() {
        if (singleton == null) {
            singleton = new ConnectionSGBD();
        }
        return singleton;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setAutoCommit(boolean b) throws SQLException {
        connection.setAutoCommit(b);
    }

    public void commit() throws SQLException {
        connection.commit();
    }

}
