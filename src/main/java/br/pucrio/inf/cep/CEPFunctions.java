package br.pucrio.inf.cep;

import org.geotools.geometry.jts.JTSFactoryFinder;

import br.pucrio.inf.domain.GeoPoI;
import br.pucrio.inf.domain.Mensagem;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

public class CEPFunctions {

    private static GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

    /**
     * Calculates the distance between two (latitude, longitude) points.
     *
     * @param referential The base latitude, longitude point.
     * @param node Other (latitude, longitude) point.
     * @return the distance (in meters) from the referential point to the other
     * one.
     */
    public static double distance(double lat, double lng, double otherLat, double otherLng) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(otherLat - lat);
        double dLng = Math.toRadians(otherLng - lng);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(otherLat))
                * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    public static double distance(Mensagem msg, GeoPoI poi) {
        Point msgPoint = geometryFactory.createPoint(new Coordinate(msg.getLongitude(), msg.getLatitude()));
        double distance = msgPoint.distance(poi.getGeometry());
        distance = (distance * (Math.PI / 180) * 6378137);

        return distance;
    }
}
