package br.pucrio.inf.cep;

import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.GeoPoI;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.PoI;
import br.pucrio.inf.domain.Rule;
import br.pucrio.inf.outputstream.StandardOutput;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.ConfigurationEngineDefaults.Threading;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EPStatementException;
import com.espertech.esper.client.soda.EPStatementObjectModel;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

public class CEPEngine {

    /**
     * LOGGER.
     */
    private static Logger LOGGER = Logger.getLogger(CEPEngine.class.getCanonicalName());

    /**
     * Singleton.
     */
    private static CEPEngine singleton;

    /**
     * CEP Engine.
     */
    private EPServiceProvider cepEngine;

    /**
     * Set of deployed rules.
     */
    private Set<Rule> deployedRules;

    /**
     * Standard Output.
     */
    private StandardOutput output;

    //////////////////////////////////////////////////////////////////////////
    // Singleton
    //////////////////////////////////////////////////////////////////////////
    private CEPEngine() {
        Configuration cepConfig = new Configuration();
        Threading threading = cepConfig.getEngineDefaults().getThreading();
        threading.setThreadPoolOutbound(true);
        threading.setThreadPoolOutboundNumThreads(4);
        
        cepConfig.addEventType(ClearEvent.class);
        cepConfig.addEventType(Mensagem.class);
        cepConfig.addEventType(Categoria.class);
        cepConfig.addEventType(GeoPoI.class);
        
        this.cepEngine = EPServiceProviderManager.getProvider("ESPER", cepConfig);
        this.deployedRules = new HashSet<>();
        this.output = StandardOutput.getInstance();

        // Basic Rules
        cepEngine.getEPAdministrator().createEPL("CREATE WINDOW MensagemWindow.win:time(1 day)  AS Mensagem");
        cepEngine.getEPAdministrator().createEPL("CREATE WINDOW PoI.win:keepall()               AS GeoPoI");

        cepEngine.getEPAdministrator().createEPL("INSERT INTO MensagemWindow  SELECT * FROM Mensagem");
        cepEngine.getEPAdministrator().createEPL("INSERT INTO PoI             SELECT * FROM GeoPoI");

        // Clear
        cepEngine.getEPAdministrator().createEPL("ON ClearEvent DELETE FROM MensagemWindow");
    }

    public static CEPEngine getInstance() {
        if (singleton == null) {
            singleton = new CEPEngine();
        }

        return singleton;
    }

    //////////////////////////////////////////////////////////////////////////
    // Rule Interface
    //////////////////////////////////////////////////////////////////////////
    /**
     * Synchronize a set of {@link Rule} instances to the CEP engine.
     *
     * @param rule a set of CEP rules.
     * @return true if the rules was successfully synchronized, false otherwise.
     */
    public synchronized boolean synchronizeRuleSet(Set<Rule> ruleSet) throws RuntimeException {        
        Set<Rule> intersectionSet = new HashSet<>(ruleSet);
        intersectionSet.retainAll(deployedRules);

        Set<Rule> toDeploy = new HashSet<>(ruleSet);
        toDeploy.removeAll(new HashSet<>(intersectionSet));

        Set<Rule> toRemove = new HashSet<>(deployedRules);
        toRemove.removeAll(new HashSet<>(intersectionSet));

        boolean added = true;
        for (Rule rule : toDeploy) {
            added = addRule(rule);
        }

        boolean removed = true;
        for (Rule rule : toRemove) {
            removed = removeRule(rule);
        }

        return added && removed;
    }

    /**
     * Add {@link Rule} to the CEP engine.
     *
     * @param rule a CEP rule.
     * @return true if the rule was successfully deployed, false otherwise.
     */
    public synchronized boolean addRule(Rule rule) throws RuntimeException {
        boolean deployed = false;

        // Check if we have this rule
        if (rule.isDisable()) {
            LOGGER.error("Rule is disable: " + rule);
        } else if (deployedRules.contains(rule)) {
            LOGGER.error("Already deployed this rule " + rule);
        } else {
            try {
                // We have a new rule, let's try to compile it
                EPStatementObjectModel ruleModel = cepEngine.getEPAdministrator().compileEPL(rule.getEpl());

                // Let's deploy it and link with the output
                cepEngine.getEPAdministrator().create(ruleModel, "R" + rule.hashCode()).addListener(output);

                // Add to deployed set
                deployedRules.add(rule);
                deployed = true;
            } catch (EPStatementException ex) {
                LOGGER.error("Error when trying to compile and deploy rule " + rule);
            }
        }

        return deployed;
    }

    /**
     * Remove a {@link Rule} from the CEP Engine
     *
     * @param rule the rule to remove
     * @return true if the rule was successfully removed, false otherwise.
     */
    public synchronized boolean removeRule(Rule rule) {
        boolean deleted = false;

        // Check if we have this rule
        if (deployedRules.contains(rule)) {
            EPStatement cepRule = cepEngine.getEPAdministrator().getStatement("R" + rule.hashCode());

            if (cepRule != null) {
                cepRule.destroy();
                deployedRules.remove(rule);

                deleted = true;
            } else {
                LOGGER.error("Rule in deployed set but does not exist in the CEP Engine" + rule);
            }
        } else {
            LOGGER.error("This rule have not been deployed" + rule);
        }

        return deleted;
    }

    public synchronized void destroyAllRules() {
        this.cepEngine.getEPAdministrator().destroyAllStatements();
        this.deployedRules = new HashSet<>();
    }

    //////////////////////////////////////////////////////////////////////////
    // Gets and Sets
    //////////////////////////////////////////////////////////////////////////
    public EPServiceProvider getCepEngine() {
        return cepEngine;
    }

    public Set<Rule> getDeployedRules() {
        return deployedRules;
    }

    public void clear() {
        cepEngine.getEPRuntime().sendEvent(new ClearEvent(""));
        StandardOutput.getInstance().clear();
    }
}
