package br.pucrio.inf.cep;

import java.io.Serializable;

public class ClearEvent implements Serializable {
    
    private String action;
    
    public ClearEvent() {}
    
    public ClearEvent(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
    
    public void setAction(String action) {
        this.action = action;
    }

}
