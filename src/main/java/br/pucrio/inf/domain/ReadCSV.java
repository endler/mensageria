/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael Pereira
 */
public class ReadCSV {

    public ArrayList<String> readCSV(String file) {
        try {
            ArrayList<String> result = new ArrayList<>();
            Scanner scanner = new Scanner(new File(file));
            scanner.useDelimiter("\n");
            while (scanner.hasNext()) {
                result.add(scanner.next());
            }
            scanner.close();
            return result;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

}
