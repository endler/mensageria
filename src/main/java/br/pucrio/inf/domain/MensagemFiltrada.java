package br.pucrio.inf.domain;

import br.pucrio.inf.database.ConnectionSGBD;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;

public class MensagemFiltrada {

    private String filter;
    private Mensagem msg;
    private int severity;
    private GeoPoI poi;

    public MensagemFiltrada() {
    }

    public MensagemFiltrada(String filter, Mensagem mensagem) {
        this(filter, mensagem, 0, null);
    }

    public MensagemFiltrada(String filter, Mensagem mensagem, int severity, GeoPoI poi) {
        this.filter = filter;
        this.msg = mensagem;
        this.severity = severity;
        this.poi = poi;
    }

    //////////////////////////////////////////////////////////////////////////
    // To GUI
    //////////////////////////////////////////////////////////////////////////
    public Object[] toRow() {
        Object[] rowArray = msg.toRow();
        rowArray = ArrayUtils.addAll(rowArray, new Object[]{msg.getId()});
        return ArrayUtils.addAll(new Object[]{filter}, Arrays.copyOfRange(rowArray, 1, rowArray.length));
    }

    //////////////////////////////////////////////////////////////////////////
    // Get and Sets
    //////////////////////////////////////////////////////////////////////////
    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Mensagem getMsg() {
        return msg;
    }

    public void setMsg(Mensagem msg) {
        this.msg = msg;
    }

    public int getSeverity() {
        return severity;
    }

    public GeoPoI getPoi() {
        return poi;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public void setPoi(GeoPoI poi) {
        this.poi = poi;
    }

    @Override
    public String toString() {
        return "FILTER: " + filter + "\t" + msg.toString();
    }

    //////////////////////////////////////////////////////////////////////////
    // JDBC
    //////////////////////////////////////////////////////////////////////////
    public boolean saveFiltered() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("select insert_mensagem_filtrada(?, ?, ?, ?, ?)");
            pps.setInt(1, getMsg().getId());
            pps.setInt(2, getMsg().getCategoryID());
            pps.setString(3, getFilter());
            if (getPoi() == null) {
                pps.setString(4, null);
            } else {
                pps.setString(4, getPoi().getRegionName());
            }
            pps.setInt(5, getSeverity());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public static boolean clearFilteredMessages() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("delete from mensagens_filtradas");
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

}
