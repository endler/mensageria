package br.pucrio.inf.domain;

import br.pucrio.inf.database.ConnectionSGBD;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Categoria implements Serializable {

    /**
     * String describing a categoria.
     */
    private String description;

    /**
     * String describing a source of categoria.
     */
    private String sourceId;

    /**
     * Categoria ID.
     */
    private int id;

    /**
     * Categoria describes a type of event in the COR system.
     *
     * @param description a String describing the events in this category
     * @param sourceId a String describing the source of category
     * @param id the category id, useful for further verifying the category
     * events.
     */
    public Categoria(String description, String sourceId, int id) {
        this.description = description;
        this.sourceId = sourceId;
        this.id = id;
    }

    /**
     * Categoria describes a type of event in the COR system.
     *
     * @param json a {@link JsonNode} represeting this category.
     * @param sourceId the source of categoria
     */
    public Categoria(JsonNode json, String sourceId) {
        this.description = json.get(0).asText();
        this.sourceId = sourceId;
        this.id = json.get(1).asInt();
    }

    //////////////////////////////////////////////////////////////////////////
    // Gets and Sets
    //////////////////////////////////////////////////////////////////////////
    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    //////////////////////////////////////////////////////////////////////////
    // JDBC
    //////////////////////////////////////////////////////////////////////////
    public boolean save() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("select insert_categoria(?, ?, ?)");
            pps.setInt(1, getId());
            pps.setString(2, getDescription());
            pps.setString(3, getSourceId());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    //////////////////////////////////////////////////////////////////////////
    // BUILDER
    //////////////////////////////////////////////////////////////////////////
    private Categoria(Builder builder) {
        this.description = builder.description;
        this.sourceId = builder.sourceId;
        this.id = builder.id;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String description;
        private String sourceId;
        private int id;

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withSourceId(String sourceId) {
            this.sourceId = sourceId;
            return this;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Categoria build() {
            return new Categoria(this);
        }
    }

    public String toRow() {
        return this.getId() + "-" + this.getDescription();
    }
}
