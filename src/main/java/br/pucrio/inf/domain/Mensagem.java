package br.pucrio.inf.domain;

import br.pucrio.inf.database.ConnectionSGBD;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.operation.distance.DistanceOp;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

public class Mensagem implements Serializable {

    private static GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

    public static void truncate() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("truncate mensagens cascade;");
            pps.execute();
        } catch (SQLException ex) {
            System.out.println("error saving message");
            Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // IDs
    protected int id;
    protected String sourceID;
    protected int categoryID;
    protected Categoria category;

    // Data
    protected String description;
    protected String icon;
    protected String title;
    protected SituacaoMensagem situation;

    // Geo
    private double latitude;
    private double longitude;
    private String geoType;

    // Metadata
    private String rawJSON;

    public Mensagem() {
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("MSG\n\t ID: ").append(id).append("\n");
        sb.append("\t Source: ").append(sourceID).append("\n");
        sb.append("\t Category: ").append(categoryID).append("\n");
        sb.append("\t Description: ").append(description).append("\n");
        sb.append("\t Title: ").append(title).append("\n");
        sb.append("\t Situation: ").append(situation).append("\n");
        sb.append("\t Geometry: ").append("lat: ").append(latitude).append(" long: ").append(longitude).append("\n");

        return sb.toString();
    }

    //////////////////////////////////////////////////////////////////////////
    // Geo
    //////////////////////////////////////////////////////////////////////////
    public double distance(GeoPoI poi) {
        Point msgPoint = geometryFactory.createPoint(new Coordinate(longitude, latitude));

        double distance = Integer.MIN_VALUE;
        // adapted from http://docs.geotools.org/stable/userguide/library/referencing/calculator.html
        try {
            CoordinateReferenceSystem crs = CRS.decode("EPSG:4326");
            GeodeticCalculator gc = new GeodeticCalculator(crs);

            gc.setStartingPosition(JTS.toDirectPosition(DistanceOp.closestPoints(poi.getGeometry(), msgPoint)[0], crs));
            gc.setDestinationPosition(JTS.toDirectPosition(msgPoint.getCoordinate(), crs));
            distance = gc.getOrthodromicDistance();
        } catch (TransformException | FactoryException e) {
            e.printStackTrace();
        }

        return distance;
    }

    public boolean contained(GeoPoI poi) {
        Point msgPoint = geometryFactory.createPoint(new Coordinate(longitude, latitude));
        return poi.getGeometry().contains(msgPoint);
    }

    //////////////////////////////////////////////////////////////////////////
    // Gets and Sets
    //////////////////////////////////////////////////////////////////////////
    public int getId() {
        return id;
    }

    public String getSourceID() {
        return sourceID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public Categoria getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public String getDescriptionToRow() {
        String output = getDescription() + "\n\n";
        output += "Latitude: " + getLatitude() + "\n";
        output += "Longitude: " + getLongitude() + "\n\n";
        return output;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public SituacaoMensagem getSituation() {
        return situation;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getGeoType() {
        return geoType;
    }

    public void setGeoType(String geoType) {
        this.geoType = geoType;
    }

    public String getRawJSON() {
        return rawJSON;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public void setCategory(Categoria category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description.replaceAll("<[^>]*>", " ");
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSituation(SituacaoMensagem situation) {
        this.situation = situation;
    }

    public void setRawJSON(String rawJSON) {
        this.rawJSON = rawJSON;
    }

    //////////////////////////////////////////////////////////////////////////
    // To GUI
    //////////////////////////////////////////////////////////////////////////
    public Object[] toRow() {
        String msg = getDescriptionToRow();
        if (description.length() > 50) {
            msg = msg.substring(0, 50) + "...";
        }
        return new Object[]{id, category.toRow(), title, msg.trim(), situation, "----"};
    }

    //////////////////////////////////////////////////////////////////////////
    // JDBC
    //////////////////////////////////////////////////////////////////////////
    public boolean save() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("INSERT INTO mensagens(\n"
                    + "            id_mensagem, descricao, \"timestamp\", id_fonte, icone, id_categoria, \n"
                    + "            situacao, json, titulo, analisada, geo_longitude, geo_latitude, \n"
                    + "            geo_tipo)\n"
                    + "    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pps.setInt(1, getId());
            pps.setString(2, getDescription());
            pps.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            pps.setString(4, getSourceID());
            pps.setString(5, getIcon());
            pps.setInt(6, getCategoryID());
            pps.setString(7, getSituation().name());
            pps.setString(8, getRawJSON());
            pps.setString(9, getTitle());
            pps.setBoolean(10, true);
            pps.setDouble(11, getLongitude());
            pps.setDouble(12, getLatitude());
            pps.setString(13, getGeoType());
            pps.execute();
        } catch (SQLException ex) {
            System.out.println("error saving message");
            if (!ex.getMessage().contains("pk")) {
                Logger.getLogger(Categoria.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            try {
                ConnectionSGBD.getInstance().getConnection().rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(Mensagem.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return true;
    }

    public boolean save(ConnectionSGBD conn) {
        try {
            PreparedStatement pps = conn.getConnection().prepareStatement("select insert_mensagem(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pps.setInt(1, getId());
            pps.setString(2, getTitle());
            pps.setString(3, getDescription());
            pps.setString(4, getSourceID());
            pps.setString(5, getIcon());
            pps.setInt(6, getCategoryID());
            pps.setString(7, getSituation().name());
            pps.setString(8, getRawJSON());
            pps.setDouble(9, getLongitude());
            pps.setDouble(10, getLatitude());
            pps.setString(11, getGeoType());
            pps.execute();
        } catch (SQLException ex) {
            System.out.println("error saving message");
            Logger
                    .getLogger(Categoria.class
                            .getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    //////////////////////////////////////////////////////////////////////////
    // Builder
    //////////////////////////////////////////////////////////////////////////
    private Mensagem(Builder builder) {
        this.id = builder.id;
        this.sourceID = builder.sourceID;
        this.categoryID = builder.categoryID;
        this.category = builder.category;
        this.description = builder.description;
        this.icon = builder.icon;
        this.title = builder.title;
        this.situation = builder.situation;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.geoType = builder.geoType;
        this.rawJSON = builder.rawJSON;
    }

    public static Builder builder() {
        return new Builder();

    }

    public static class Builder {

        protected int id;
        protected String sourceID;
        protected int categoryID;
        protected Categoria category;
        protected String description;
        protected String icon;
        protected String title;
        protected SituacaoMensagem situation;
        private double latitude;
        private double longitude;
        private String geoType;
        private String rawJSON;

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withSourceID(String sourceID) {
            this.sourceID = sourceID;
            return this;
        }

        public Builder withCategoryID(int categoryID) {
            this.categoryID = categoryID;
            return this;
        }

        public Builder withCategory(Categoria category) {
            this.category = category;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description.replaceAll("<[^>]*>", " ");
            return this;
        }

        public Builder withIcon(String icon) {
            this.icon = icon;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withSituation(SituacaoMensagem situation) {
            this.situation = situation;
            return this;
        }

        public Builder withLatitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder withLongitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder withGeoType(String geoType) {
            this.geoType = geoType;
            return this;
        }

        public Builder withRawJSON(String rawJSON) {
            this.rawJSON = rawJSON;
            return this;
        }

        public Mensagem build() {
            return new Mensagem(this);
        }
    }
}
