package br.pucrio.inf.domain;

import br.pucrio.inf.database.ConnectionSGBD;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PoI implements Serializable {

    private double latitude;
    private double longitude;
    private String type;
    private int order;
    private String region;

    public PoI() {
    }

    public PoI(double lat, double lng) {
        this.latitude = lat;
        this.longitude = lng;
    }

    public PoI(String region, double latitude, double longitude, int order, String type) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.region = region;
        this.order = order;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return type + " (" + latitude + ", " + longitude + ")";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean save() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("INSERT INTO public.poi(regiao, latitude, longitude, ordem) VALUES (?, ?, ?, ?);");
            pps.setString(1, region);
            pps.setDouble(2, latitude);
            pps.setDouble(3, longitude);
            pps.setInt(4, order);
            pps.execute();
        } catch (SQLException ex) {
            return false;
        }
        return true;

    }

}
