package br.pucrio.inf.domain;

import br.pucrio.inf.database.ConnectionSGBD;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Rule {

    private int id;
    private String epl;
    private String name;
    private SituacaoRegra status;

    public Rule() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(SituacaoRegra status) {
        this.status = status;
    }

    public SituacaoRegra getStatus() {
        return status;
    }

    public Rule(int id, String epl, String name, SituacaoRegra status) {
        this.id = id;
        this.epl = epl;
        this.name = name;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Rule (" + id + "/" + name + "): " + epl;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.id;
        hash = 67 * hash + Objects.hashCode(this.epl);
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rule other = (Rule) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.epl, other.epl)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        return true;
    }

    //////////////////////////////////////////////////////////////////////////
    // Gets and Sets
    //////////////////////////////////////////////////////////////////////////
    public String getEpl() {
        return epl;
    }

    public int getId() {
        return id;
    }

    public void setEpl(String epl) {
        this.epl = epl;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object[] toRow() {
        return new Object[]{id, name, epl, status};
    }

    public void save() {
        if (this.getId() <= 0) {
            this.insert();
        } else {
            this.edit();
        }
    }

    private void insert() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("insert into regras (regra, nome, situacao) values (?, ?, ?)");
            pps.setString(1, getEpl());
            pps.setString(2, getName());
            pps.setString(3, "ativa");
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Rule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void edit() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("update regras set regra = ?, nome = ? where id_regra = ?");
            pps.setString(1, getEpl());
            pps.setString(2, getName());
            pps.setInt(3, getId());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Rule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("delete from regras where id_regra = ?");
            pps.setInt(1, getId());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Rule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeStatus() {
        try {
            ConnectionSGBD conn = ConnectionSGBD.getInstance();
            PreparedStatement pps = conn.getConnection().prepareStatement("update regras set situacao = '" + status.reverse() + "' where id_regra = ?");
            pps.setInt(1, getId());
            pps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Rule.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isDisable() {
        boolean resultado = status.equals(SituacaoRegra.inativa);
        return resultado;
    }

    public SituacaoRegra getReverseSituation() {
        return status.reverse();
    }

}
