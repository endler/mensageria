package br.pucrio.inf.domain;

public enum SituacaoRegra {
    ativa, inativa;

    public SituacaoRegra reverse() {
        if (this == ativa) {
            return inativa;
        } else {
            return ativa;
        }
    }
}
