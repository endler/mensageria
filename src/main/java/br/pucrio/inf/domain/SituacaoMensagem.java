package br.pucrio.inf.domain;

public enum SituacaoMensagem {
    ABERTO, RESOLVENDO, RESOLVIDO;
}
