package br.pucrio.inf.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 *
 * @author rpoat
 */
public class Configuration {

    /** LOGGER. */
    private static Logger LOGGER = Logger.getLogger(Configuration.class.getCanonicalName());

    
    public static Properties getProperties() {
        Properties prop;
        Properties parameters = new Properties();
        try {
            prop = readPropertyFile("credentials.properties");
            if (prop != null) {
                String fileLocation = prop.getProperty("location");
                parameters = readPropertyFile(fileLocation);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return parameters;
    }
    
    private static Properties readPropertyFile(String filename) throws IOException {
        Properties properties = new Properties();
        
        // Try to load indicated property file
        File propertyFile = new File(filename);
        
        if (!propertyFile.exists()) {
            LOGGER.warn("File " + filename + " not found in the current directory");
            LOGGER.warn("Trying to load " + filename + " using the classpath");
            
            InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
            if (is != null) {
                properties.load(is);
            } else {
                LOGGER.error("File " + filename + " does not exist");
                throw new FileNotFoundException();
            }
        } else {
            FileInputStream fi = new FileInputStream(propertyFile);
            LOGGER.info("Loaded parameters from file: " + propertyFile.getName());
            properties.load(fi);
            fi.close();
        }
        
        return properties;
    }
}
