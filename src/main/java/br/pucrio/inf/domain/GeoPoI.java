package br.pucrio.inf.domain;

import java.io.Serializable;

import com.vividsolutions.jts.geom.Geometry;

public class GeoPoI implements Serializable {

    private String regionName;
    private Geometry geometry;

    public GeoPoI() {}
    
    public GeoPoI(String regionName, Geometry geometry) {
        this.regionName = regionName;
        this.geometry = geometry;
    }

    public String getRegionName() {
        return regionName;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

}
