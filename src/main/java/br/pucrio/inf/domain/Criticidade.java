package br.pucrio.inf.domain;

public enum Criticidade {
    DESPREZIVEL, MODERADO, SEVERO, CRITICO;
}
