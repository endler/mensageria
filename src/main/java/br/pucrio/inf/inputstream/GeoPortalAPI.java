package br.pucrio.inf.inputstream;

import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.SituacaoMensagem;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.security.auth.login.CredentialException;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.log4j.Logger;

/**
 * This class provides methods for consuming and interacting with the Rio de
 * Janeiro COR GeoPortal API. It requires a key to access the API and support
 * the following methods: lista and categoria.
 */
public class GeoPortalAPI {

    /**
     * LOGGER.
     */
    private static Logger LOGGER = Logger.getLogger(GeoPortalAPI.class.getCanonicalName());

    /**
     * GeoPortal key (chave)
     */
    private String key;

    // TODO: Rever isso aqui.
    /**
     * GeoPortal source ID
     */
    private String sourceID = "1";

    /**
     * HTTP Client (to consume the REST API)
     */
    private OkHttpClient httpClient;

    /**
     * JSON Mapper to deserialize the HTTP calls
     */
    private ObjectMapper jsonMapper;

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param credentials the API key (property 'chave').
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public GeoPortalAPI(Properties credentials) throws CredentialException, IOException {
        this((String) credentials.get("chave"));
    }

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param key a String representing the API key.
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public GeoPortalAPI(String key) throws CredentialException, IOException {
        this.key = key;
        this.httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true).build();
        this.jsonMapper = new ObjectMapper();

        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        checkKeyValidity(key);
    }

    /**
     * This method verify if the provided API key is valid.
     *
     * @param key the api key (chave)
     * @return true if the key is valid, false otherwise.
     *
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public boolean checkKeyValidity(String key) throws IOException {
        HttpUrl httpURL = baseURLBuilder().build();

        Request request = new Request.Builder().url(httpURL).build();
        Response response = httpClient.newCall(request).execute();

        String json = response.body().string();
        if ((json.toLowerCase().contains("invalida")) || (json.isEmpty()) || (json.equalsIgnoreCase(""))) {
            LOGGER.error("Incorrect or Malformated key to access the GeoPortal API");
            return false;
        } else {
            LOGGER.info("Successfully connected to the GeoPortal API");
            return true;
        }
    }

    /**
     * This method list the available {@link Categoria}s in GeoPortal
     *
     * @return a List of {@link Categoria}s
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public List<Categoria> listCategories() throws IOException {
        List<Categoria> categories = new ArrayList<>();

        HttpUrl httpURL = baseURLBuilder()
                .addQueryParameter("metodo", "lista")
                .build();

        long before = System.currentTimeMillis();

        Request request = new Request.Builder().url(httpURL).build();
        Response response = httpClient.newCall(request).execute();

        long after = System.currentTimeMillis();
        LOGGER.info("HTTP DELAY FOR LIST CATEGORIES REQUEST: " + (after - before) + " ms");

        String rawJSON = response.body().string();
        JsonNode rawNode = jsonMapper.readValue(rawJSON, JsonNode.class);

        ArrayNode rawCategoriesArray = (ArrayNode) rawNode.get("DATA");
        Iterator<JsonNode> iterator = rawCategoriesArray.iterator();

        while (iterator.hasNext()) {
            categories.add(new Categoria(iterator.next(), "1"));
        }

        return categories;
    }

    /**
     * This method get the available messages ({@link Mensagem}) of a given
     * {@link Categoria} in the GeoPortal API.
     *
     * @param categoria a given {@link Categoria}
     * @return a List of messages
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public List<Mensagem> getMessages(Categoria categoria) throws IOException {
        System.out.println("rafael getMessages");
        List<Mensagem> messages = new ArrayList<>();

        HttpUrl httpURL = baseURLBuilder()
                .addQueryParameter("metodo", "categoria")
                .addQueryParameter("formato", "json")
                .addQueryParameter("categoria", categoria.getId() + "")
                .build();

        long before = System.currentTimeMillis();

        Request request = new Request.Builder().url(httpURL).build();
        Response response = httpClient.newCall(request).execute();

        long after = System.currentTimeMillis();
        LOGGER.info("HTTP DELAY FOR MESSAGE REQUEST (CATEGORY " + categoria.getId() + "): " + (after - before) + " ms");

        String rawJSON = response.body().string();
        JsonNode rawNode = jsonMapper.readValue(rawJSON, JsonNode.class);
        ArrayNode rawFeatures = (ArrayNode) rawNode.get("features");

        Iterator<JsonNode> rawMSGIterator = rawFeatures.iterator();
        while (rawMSGIterator.hasNext()) {
            JsonNode rawMSG = rawMSGIterator.next();
            JsonNode rawProperties = rawMSG.get("properties");

            // Geometry
            ArrayNode rawGeometryArray = (ArrayNode) rawMSG.get("geometry").get("geometries");
            Iterator<JsonNode> rawGeometryIterator = rawGeometryArray.iterator();

            double longitude = 0, latitude = 0;
            String type = null;
            while (rawGeometryIterator.hasNext()) {
                JsonNode rawGeometry = rawGeometryIterator.next();
                longitude = rawGeometry.get("coordinates").get(0).asDouble();
                latitude = rawGeometry.get("coordinates").get(1).asDouble();
                type = rawGeometry.get("type").asText();
            }

            Mensagem.Builder msgBuilder = new Mensagem.Builder();

            msgBuilder.withId(rawProperties.get("id").asInt())
                    .withSourceID(this.sourceID)
                    .withCategoryID(categoria.getId())
                    .withCategory(categoria)
                    .withDescription(rawProperties.get("descricao").asText())
                    .withIcon(rawProperties.get("icone").asText())
                    .withTitle(rawProperties.get("titulo").asText())
                    .withSituation(SituacaoMensagem.ABERTO)
                    .withRawJSON(rawMSG.toString())
                    .withLatitude(latitude)
                    .withLongitude(longitude)
                    .withGeoType(type);
            messages.add(msgBuilder.build());
        }
        return messages;

    }

    /**
     * A utility method for building the GeoPortal URL call.
     *
     * @return a http url builder
     */
    private HttpUrl.Builder baseURLBuilder() {
        HttpUrl.Builder httpURLBuilder = new HttpUrl.Builder()
                .scheme("http")
                .host("geoportal.cor.rio.gov.br")
                .addPathSegments("webservices/API.cfm")
                .addQueryParameter("chave", key);
        return httpURLBuilder;
    }

}
