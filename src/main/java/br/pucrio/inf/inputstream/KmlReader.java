/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf.inputstream;

import br.pucrio.inf.domain.PoI;
import de.micromata.opengis.kml.v_2_2_0.Boundary;
import de.micromata.opengis.kml.v_2_2_0.Container;
import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.LinearRing;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Polygon;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rafael
 */
public class KmlReader {

    public void parseFeature(Feature feature) {
        if (feature != null) {
            if (feature instanceof Document) {
                Document document = (Document) feature;
                List<Placemark> placemarkList = getPlacemarks(document, "");
                for (Placemark placemark : placemarkList) {
                    Geometry geometry = placemark.getGeometry();
                    parseGeometry(geometry, placemark.getDescription() + "/" + placemark.getName());
                }
            }
        }
    }

    public ArrayList<Placemark> getPlacemarks(Feature root, String folder) {
        ArrayList<Placemark> Placemarks = new ArrayList<Placemark>();
        if (root instanceof Container) {
            if (root instanceof Document) {
                for (Feature feature : ((Document) root).getFeature()) {
                    if (feature instanceof Placemark) {
                        Placemarks.add((Placemark) feature);
                    } else if ((feature instanceof Document)
                            || (feature instanceof Folder)) {
                        if (!feature.getName().equals("Info")) {
                            Placemarks.addAll(getPlacemarks(feature, formatName(feature.getName(), folder)));
                        }
                    }
                }
            } else if (root instanceof Folder) {
                for (Feature feature : ((Folder) root).getFeature()) {
                    if (feature instanceof Placemark) {
                        ((Placemark) feature).setDescription(folder);
                        Placemarks.add((Placemark) feature);
                    } else if ((feature instanceof Document)
                            || (feature instanceof Folder)) {
                        if (!feature.getName().equals("Info")) {
                            Placemarks.addAll(getPlacemarks(feature, formatName(feature.getName(), folder)));
                        }
                    }
                }
            }
        } else if (root instanceof Placemark) {
            Placemarks.add((Placemark) root);
        }
        return Placemarks;
    }

    private void parseGeometry(Geometry geometry, String region) {
        if (geometry != null) {
            if (geometry instanceof Polygon) {
                Polygon polygon = (Polygon) geometry;
                Boundary outerBoundaryIs = polygon.getOuterBoundaryIs();
                if (outerBoundaryIs != null) {
                    LinearRing linearRing = outerBoundaryIs.getLinearRing();
                    if (linearRing != null) {
                        List<Coordinate> coordinates = linearRing.getCoordinates();
                        if (coordinates != null) {
                            int i = 1;
                            for (Coordinate coordinate : coordinates) {
                                parseCoordinate(coordinate, region, i++);
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean parseCoordinate(Coordinate coordinate, String region, int pos) {
        if (coordinate != null) {
            System.out.println("REGIAO: " + region);
            System.out.println("Longitude: " + coordinate.getLongitude());
            System.out.println("Latitude : " + coordinate.getLatitude());
            System.out.println("Altitude : " + coordinate.getAltitude());
            System.out.println("POSICAO: " + pos);
            System.out.println("");
            System.out.println();

            PoI poi = new PoI(region, coordinate.getLatitude(), coordinate.getLongitude(), pos, "geometry");
            return poi.save();
        } else {
            return false;
        }
    }

    private String formatName(String name, String folder) {
        System.out.println("folder:" + name);
        if (folder.isEmpty()) {
            if (!name.equals("Lugares temporários")) {
                return name;
            } else {
                return folder;
            }
        } else if (name.equals("Border")) {
            return folder;
        } else {
            return folder + "/" + name;
        }
    }

}
