/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf.inputstream;

import br.pucrio.inf.database.ConnectionSGBD;
import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.ReadCSV;
import br.pucrio.inf.domain.SituacaoMensagem;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.security.auth.login.CredentialException;
import okhttp3.OkHttpClient;
import org.apache.log4j.Logger;

/**
 *
 * @author Rafael Pereira
 */
public class PerformanceAPI {

    /**
     * LOGGER.
     */
    private static Logger LOGGER = Logger.getLogger(PerformanceAPI.class.getCanonicalName());

    /**
     * GeoPortal key (chave)
     */
    private String key;

    // TODO: Rever isso aqui.
    /**
     * GeoPortal source ID
     */
    private String sourceID = "1";

    /**
     * HTTP Client (to consume the REST API)
     */
    private OkHttpClient httpClient;

    /**
     * JSON Mapper to deserialize the HTTP calls
     */
    private ObjectMapper jsonMapper;

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param credentials the API key (property 'chave').
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public PerformanceAPI(Properties credentials) throws CredentialException, IOException {
        this((String) credentials.get("chave"));
    }

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param key a String representing the API key.
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public PerformanceAPI(String key) throws CredentialException, IOException {
        this.key = key;
        this.httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true).build();
        this.jsonMapper = new ObjectMapper();

        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * This method list the available {@link Categoria}s in GeoPortal
     *
     * @return a List of {@link Categoria}s
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public List<Categoria> listCategories() throws IOException {
        List<Categoria> categories = new ArrayList<>();

        long before = System.currentTimeMillis();
        Categoria cat = new Categoria("TWITER", "1", 1);
        categories.add(cat);
        long after = System.currentTimeMillis();
        LOGGER.info("RP HTTP DELAY FOR LIST CATEGORIES REQUEST: " + (after - before) + " ms");

        return categories;
    }

    /**
     * This method get the available messages ({@link Mensagem}) of a given
     * {@link Categoria} in the GeoPortal API.
     *
     * @param categoria a given {@link Categoria}
     * @return a List of messages
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public List<Mensagem> getMessages(Categoria categoria) throws IOException, SQLException {
        List<Mensagem> messages = new ArrayList<>();

        ReadCSV reader = new ReadCSV();
        ArrayList<String> result = reader.readCSV("dataset/output.csv");
        System.out.println(result.size());
        int i = 0;
        Mensagem.truncate();

        long before = System.currentTimeMillis();
        long after;
        PrintWriter writer = new PrintWriter("mensageria_test_performance.csv", "UTF-8");
        writer.println("msg amount,time(s)");
        PrintWriter writer_msg = new PrintWriter(new FileOutputStream(new File("msg.txt"), true));
        ConnectionSGBD conn = ConnectionSGBD.getInstance();
        conn.setAutoCommit(true);
        for (int j = 0; j < 50; j++) {
            try {
                for (String twitt : result) {
                    i++;
                    if (i % 5000000 == 0) {
//                        conn.commit();
                        after = System.currentTimeMillis();
                        LOGGER.info("MESSAGE " + String.valueOf(i) + " time: " + (after - before) + " ms");
                        writer.println(String.valueOf(i) + " msgs," + ((after - before) * 0.001));
                    }
                    String[] twit_msg = twitt.split(",");

                    Mensagem.Builder msgBuilder = new Mensagem.Builder();

                    msgBuilder.withId(i)
                            .withSourceID(this.sourceID)
                            .withCategoryID(categoria.getId())
                            .withCategory(categoria)
                            .withDescription(twit_msg[5].replace("\"", ""))
                            .withIcon("")
                            .withTitle(twit_msg[3].replace("\"", ""))
                            .withSituation(SituacaoMensagem.ABERTO)
                            .withRawJSON("")
                            .withLatitude(100)
                            .withLongitude(100)
                            .withGeoType("twitter");
//                    messages.add(msgBuilder.build());
                    msgBuilder.build().save(conn);
//                    messages.get(i - 1).save(conn);
//                    writer_msg.println(msgBuilder.build().toString());
                }
//                conn.commit();
                after = System.currentTimeMillis();
                LOGGER.info("RP HTTP DELAY FOR MESSAGE REQUEST (CATEGORY " + categoria.getId() + "): " + (after - before) + " ms");
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        writer_msg.close();
        writer.close();
        return messages;
    }

}
