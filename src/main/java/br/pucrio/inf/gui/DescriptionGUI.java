package br.pucrio.inf.gui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.MensagemFiltrada;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;

public class DescriptionGUI extends JFrame {

    private JPanel contentPane;
    private JLabel txTitle;
    private JTextArea txDescription;

    /**
     * Launch the application.
     */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					DescriptionGUI frame = new DescriptionGUI();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
    /**
     * Create the frame.
     */
    public DescriptionGUI(Mensagem m) {
        initComponents();
        txTitle.setText(m.getTitle());
        txDescription.setText(m.getDescription());
    }

    public DescriptionGUI(MensagemFiltrada mf) {
        initComponents();
        txTitle.setText(mf.getMsg().getTitle());
        txDescription.setText(mf.getMsg().getDescriptionToRow());
    }

    private void initComponents() {
        setTitle("Descrição");
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 600, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);

        JLabel lblTtulo = new JLabel("Título:");

        txTitle = new JLabel();

        txDescription = new JTextArea();
        txDescription.setLineWrap(true);
        txDescription.setWrapStyleWord(true);
        GroupLayout gl_contentPane = new GroupLayout(contentPane);
        gl_contentPane.setHorizontalGroup(
                gl_contentPane.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_contentPane.createSequentialGroup()
                        .addComponent(lblTtulo)
                        .addPreferredGap(ComponentPlacement.RELATED)
                        .addComponent(txTitle)
                        .addContainerGap(333, Short.MAX_VALUE))
                .addComponent(txDescription, GroupLayout.DEFAULT_SIZE, 424, Short.MAX_VALUE)
        );
        gl_contentPane.setVerticalGroup(
                gl_contentPane.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_contentPane.createSequentialGroup()
                        .addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblTtulo)
                                .addComponent(txTitle))
                        .addPreferredGap(ComponentPlacement.RELATED)
                        .addComponent(txDescription, GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE))
        );
        contentPane.setLayout(gl_contentPane);
    }
}
