package br.pucrio.inf.gui;

import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.MensagemFiltrada;
import br.pucrio.inf.domain.Rule;
import br.pucrio.inf.domain.SituacaoRegra;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import net.miginfocom.swing.MigLayout;

public class DemoGUI extends JFrame {

    private JPanel contentPane;
    private JTable inputTable;
    private JTable filterTable;
    private JTable outputTable;
    private Map<Integer, Mensagem> mapMensagensInput;
    private Map<Integer, MensagemFiltrada> mapMensagensOutput;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    DemoGUI frame = new DemoGUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public DemoGUI() {
        mapMensagensInput = new HashMap<Integer, Mensagem>();
        mapMensagensOutput = new HashMap<Integer, MensagemFiltrada>();
        setTitle("Mensageria v0.1 ALPHA");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1024, 768);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(new GridLayout(0, 1, 0, 0));

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        panel.add(tabbedPane);

        JPanel inputPanel = new JPanel();
        tabbedPane.addTab("Entrada", null, inputPanel, null);
        inputPanel.setLayout(new GridLayout(1, 0, 0, 0));

        JScrollPane inputScrollPane = new JScrollPane();
        inputPanel.add(inputScrollPane);

        inputTable = new DemoTable();
        inputTable.setModel(new DefaultTableModel(new Object[][]{},
                new String[]{"ID", "Criticidade", "Categoria", "T\u00EDtulo", "Descri\u00E7\u00E3o", "Situação", "Hora"}) {
            boolean[] columnEditables = new boolean[]{false, false, false, false, false, false, false};

            @Override
            public boolean isCellEditable(int row, int column) {
                return columnEditables[column];
            }
        }
        );
        inputTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        inputTable.setAutoCreateRowSorter(true);

        inputTable.getTableHeader().setReorderingAllowed(false);

        inputTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable target = (JTable) e.getSource();
                    int row = target.convertRowIndexToModel(target.getSelectedRow());
                    Mensagem m = findMessageByIdInMap(Integer.valueOf(target.getModel().getValueAt(row, 0).toString()));
                    DescriptionGUI dGui = new DescriptionGUI(m);
                    dGui.setVisible(true);
                }
            }
        });

        inputScrollPane.setViewportView(inputTable);

        JPanel filteredPanel = new JPanel();
        tabbedPane.addTab("Filtros", null, filteredPanel, null);
        filteredPanel.setLayout(new MigLayout("filly", "[grow]", "[]10[min!]"));

        JScrollPane filterScrollPane = new JScrollPane();
        filteredPanel.add(filterScrollPane, "cell 0 0,grow");

        filterTable = new DemoTable();
        filterTable.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "ID", "Nome", "EPL", "SITUAÇÃO"
                }
        ));
        filterTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        filterTable.setAutoCreateRowSorter(true);

        filterScrollPane.setViewportView(filterTable);

        JPanel filterFooterPanel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) filterFooterPanel.getLayout();
        flowLayout.setAlignment(FlowLayout.RIGHT);
        filteredPanel.add(filterFooterPanel, "cell 0 1,grow");

        JButton createRuleButton = new JButton("Criar Regra");
        filterFooterPanel.add(createRuleButton);

        createRuleButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                RuleGUI ruleGUI = new RuleGUI(new Rule());
                ruleGUI.setVisible(true);
            }

        });

        filterTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                JTable target = (JTable) e.getSource();
                int row = target.getSelectedRow();
                RuleGUI ruleGUI = new RuleGUI(new Rule(Integer.valueOf(target.getValueAt(row, 0).toString()), target.getValueAt(row, 2).toString(), target.getValueAt(row, 1).toString(), SituacaoRegra.valueOf(target.getValueAt(row, 3).toString())));
                ruleGUI.setVisible(true);
            }
        });
        
        filterTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        JPanel outputPanel = new JPanel();
        tabbedPane.addTab("Saída", null, outputPanel, null);
        outputPanel.setLayout(new GridLayout(0, 1, 0, 0));

        JScrollPane outputScrollPane = new JScrollPane();
        outputPanel.add(outputScrollPane);

        outputTable = new DemoTable();        
        outputScrollPane.setViewportView(outputTable);

        outputTable.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{"Filtro", "Criticidade", "Categoria", "T\u00EDtulo", "Descri\u00E7\u00E3o", "Situação", "Hora", "ID"}) {
            boolean[] columnEditables = new boolean[]{false, false, false, false, false, false, false};

            public boolean isCellEditable(int row, int column) {
                return columnEditables[column];
            }
        });
        outputTable.setAutoCreateRowSorter(true);

        outputTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable target = (JTable) e.getSource();
                    int row = target.convertRowIndexToModel(target.getSelectedRow());
                    MensagemFiltrada mf = findFilteredMessageByIdInMap((int) target.getModel().getValueAt(row, 7));
                    DescriptionGUI dGui = new DescriptionGUI(mf);
                    dGui.setVisible(true);
                }
            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // Insert/Remove Rows
    // TODO: Refactor this after demo
    //////////////////////////////////////////////////////////////////////////////////////
    public void addInputMessages(List<Mensagem> mensagens) {
        this.mapMensagensInput.clear();
        DefaultTableModel model = (DefaultTableModel) inputTable.getModel();
        clearTableModel(model);

        for (Mensagem msg : mensagens) {
            model.addRow(msg.toRow());
            this.mapMensagensInput.put(msg.getId(), msg);
        }
    }

    public Mensagem findMessageByIdInMap(int id) {
        return this.mapMensagensInput.get(id);
    }

    public void addOutputMessages(Deque<MensagemFiltrada> msgOutputQueue) {
        this.mapMensagensOutput.clear();
        DefaultTableModel model = (DefaultTableModel) outputTable.getModel();
        clearTableModel(model);

        for (MensagemFiltrada msgFiltrada : msgOutputQueue) {
            model.addRow(msgFiltrada.toRow());
            this.mapMensagensOutput.put(msgFiltrada.getMsg().getId(), msgFiltrada);
        }
    }

    public MensagemFiltrada findFilteredMessageByIdInMap(int id) {
        return this.mapMensagensOutput.get(id);
    }

    public int getInputTableRowCount() {
        return inputTable.getRowCount();
    }

    public int getOutputTableRowCount() {
        return outputTable.getRowCount();
    }

    public int getFilterTableRowCount() {
        return filterTable.getRowCount();
    }

    public void clearTableModel(DefaultTableModel model) {
        model.setRowCount(0);
    }

    public void addInputFilters(Set<Rule> rules) {
        DefaultTableModel model = (DefaultTableModel) filterTable.getModel();
        clearTableModel(model);

        for (Rule rule : rules) {
            model.addRow(rule.toRow());
        }
        filterTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    }

    public void clearTableModel(String tableName) {
        switch (tableName) {
            case "inputMenssages":
                this.clearTableModel((DefaultTableModel) inputTable.getModel());
                break;
            case "filterTable":
                this.clearTableModel((DefaultTableModel) filterTable.getModel());
                break;
            case "outputTable":
                this.clearTableModel((DefaultTableModel) outputTable.getModel());
                break;

        }
    }

    public void addInputMessage(Mensagem msg) {
        DefaultTableModel model = (DefaultTableModel) inputTable.getModel();
        model.addRow(msg.toRow());
    }

    public void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 50; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width + 1, width);
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
        table.repaint();
    }

    public class DemoTable extends JTable {

        @Override
        public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
            Component component = super.prepareRenderer(renderer, row, column);
            int rendererWidth = component.getPreferredSize().width;
            TableColumn tableColumn = getColumnModel().getColumn(column);
            tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
            return component;
        }
    };

}
