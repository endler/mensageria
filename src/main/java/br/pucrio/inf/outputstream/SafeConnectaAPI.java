package br.pucrio.inf.outputstream;

import java.io.IOException;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import javax.security.auth.login.CredentialException;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.Rule;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * This class provides operations for pushing the Mensagerias filtered messages
 * to the SafeConnecta API.
 *
 * To do so, this class follows the Observer pattern and subscribes to filtered
 * messages of given rules. When the engine filters a message it will trigger
 * the {@link #update(Rule, Mensagem)} method to dispatch the detected message.
 * This method contains both, the filtered message and the rule that detected
 * it.
 *
 * If during execution, the user decide to destroy a rule, the engine will
 * trigger the {@link #destroy(Rule)} method to enable the object to clean
 * resources (e.g., store unacknowledged messages).
 *
 * Note: the API methods are located at:
 * http://corapi.gpsconecta.com.br/app/home/
 */
public class SafeConnectaAPI implements OutputObserver {

    /**
     * LOGGER.
     */
    private static Logger LOGGER = Logger.getLogger(SafeConnectaAPI.class.getCanonicalName());

    //////////////////////////////////////////////////////////////////////////
    // HTTP API variables
    //////////////////////////////////////////////////////////////////////////
    /**
     * HTTP Client (to push the output message).
     */
    private OkHttpClient httpClient;

    /**
     * JSON Mapper to serialize the message in JSON.
     */
    private ObjectMapper jsonMapper;

    /**
     * API username. Used to create the API token (key).
     */
    private String username;

    /**
     * API password. Used to create the API token (key).
     */
    private String password;

    /**
     * API Key (token). Resets every 24h.
     */
    private String key;

    /**
     * Timestamp of last key update.
     */
    private long lastKeyUpdate;

    //////////////////////////////////////////////////////////////////////////
    // OBSERVER variables
    //////////////////////////////////////////////////////////////////////////
    private Queue<Pair<Rule, Mensagem>> unackMSG;

    //////////////////////////////////////////////////////////////////////////
    // Constructors
    //////////////////////////////////////////////////////////////////////////
    /**
     * SafeConnecta API Constructor. The SafeConnecta API requires the use of
     * credentials. Thus, when creating the object the developers shall pass as
     * argument a properties object containing a {@link Properties} object or
     * the credential user and password as {@link String} using the following
     * keys: safeconnectauser and safeconnectapass, respectively.
     *
     * @param properties a {@link Properties} object containing the credentials
     * keys: safeconnectauser, safeconnectapass
     * @throws CredentialException if the credential is invalid.
     * @throws NullPointerException if the credential is null.
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected) during the validation.
     */
    public SafeConnectaAPI(Properties properties) throws CredentialException, NullPointerException, IOException {
        this(properties.getProperty("safeconnectauser"), properties.getProperty("safeconnectapass"));
    }

    /**
     * SafeConnecta API Constructor. The SafeConnecta API requires the use of
     * credentials. Thus, when creating the object the developers shall pass as
     * argument a properties object containing a {@link Properties} object or
     * the credential user and password as {@link String} using the following
     * keys: safeconnectauser and safeconnectapass, respectively.
     *
     * @param username the API username
     * @param password the API email
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected) during the validation.
     */
    public SafeConnectaAPI(String username, String password) throws CredentialException, IOException {
        this.username = username;
        this.password = password;
        this.httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true).build();
        this.jsonMapper = new ObjectMapper();
        this.unackMSG = new LinkedBlockingDeque<>();

        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        if (!validateCredential(username, password)) {
            String errorMSG = "Incorrect credentials (username, password): " + username + ":" + password;
            LOGGER.error(errorMSG);
            throw new CredentialException(errorMSG);
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // OBSERVER
    //////////////////////////////////////////////////////////////////////////
    @Override
    public void update(Rule rule, Mensagem msg) {
        JsonNode msgOutput = toJsonOutput(rule, msg);

    }

    @Override
    public void destroy(Rule rule) {
    }

    //////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////
    /**
     * Transform the filtered message to its SafeConnecta JSON output.
     *
     * @param rule the rule that filtered the message
     * @param msg the filtered message
     * @return a {@link JsonNode} representing the message as JSON.
     */
    private JsonNode toJsonOutput(Rule rule, Mensagem msg) {
        ObjectNode json = jsonMapper.createObjectNode();

        json.put("id", msg.getId());
        json.put("category", msg.getCategoryID());
        json.put("latitude", msg.getLatitude());
        json.put("longitude", msg.getLongitude());
        json.put("date", ""); //TODO: Definir data

        json.put("description", msg.getDescription());

        json.put("title", msg.getTitle());
        json.put("ruleName", rule.getName());

        return json;
    }

    /**
     * A utility method for building the SafeConnecta PUSH URL call.
     *
     * @return a http url builder
     */
    private HttpUrl.Builder baseURLBuilder() {
        HttpUrl.Builder httpURLBuilder = new HttpUrl.Builder()
                .scheme("http")
                .host("geoportal.cor.rio.gov.br")
                .addPathSegments("webservices/API.cfm")
                .addQueryParameter("chave", key);
        return httpURLBuilder;
    }

    /**
     * This method verify if the provided API key is valid.
     *
     * @param username the API username
     * @param password the API email
     * @return true if the key is valid, false otherwise.
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected) during the validation.
     */
    private boolean validateCredential(String username, String password) throws IOException {
        boolean valid = false;

        RequestBody formBody = new FormBody.Builder()
                .add("grant_type", "password")
                .add("username", username)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url("http://corapi.gpsconecta.com.br/api/security/token")
                .post(formBody)
                .build();

        Response response = httpClient.newCall(request).execute();

        if (response.code() == 200) {
            String rawJSON = response.body().string();
            JsonNode rawNode = jsonMapper.readValue(rawJSON, JsonNode.class);

            key = rawNode.get("access_token").asText();
            lastKeyUpdate = System.currentTimeMillis();

            valid = true;
        }

        return valid;
    }
}
