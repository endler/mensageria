/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf.outputstream;

import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.MensagemFiltrada;
import br.pucrio.inf.domain.Rule;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import javax.security.auth.login.CredentialException;
import okhttp3.OkHttpClient;
import org.apache.log4j.Logger;

/**
 * This class provides operations for pushing the filtered messages to the
 * GeoPortal API.
 *
 * To do so, this class follows the Observer pattern and subscribes to filtered
 * messages of given rules. When the engine filters a message it will trigger
 * the {@link #update(Rule, Mensagem)} method to dispatch the detected message.
 * This method contains both, the filtered message and the rule that detected
 * it.
 */
public class PerformancelPushAPI {

    /**
     * LOGGER.
     */
    private static Logger LOGGER = Logger.getLogger(PerformancelPushAPI.class.getCanonicalName());

    /**
     * GeoPortal key (chave)
     */
    private String key;

    // TODO: Rever isso aqui.
    /**
     * GeoPortal source ID
     */
    private String sourceID = "1";

    /**
     * HTTP Client (to consume the REST API)
     */
    private OkHttpClient httpClient;

    /**
     * JSON Mapper to deserialize the HTTP calls
     */
    private ObjectMapper jsonMapper;

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param credentials the API key (property 'chave').
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public PerformancelPushAPI(Properties credentials) throws CredentialException, IOException {
        this((String) credentials.get("chave"));
    }

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param key a String representing the API key.
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public PerformancelPushAPI(String key) throws CredentialException, IOException {

    }

    /**
     * This method verify if the provided API key is valid.
     *
     * @param key the api key (chave)
     * @return true if the key is valid, false otherwise.
     *
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public boolean checkKeyValidity(String key) throws IOException {
        return true;
    }

    /**
     * This method push the messages to the GeoPortal API
     *
     * @param msgs
     * @return a List of {@link Categoria}s
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public boolean pushMessages(Collection<MensagemFiltrada> msgs) throws IOException {
        return true;
    }

    /**
     * A utility method for building the GeoPortal URL call.
     *
     * @return a http url builder
     */
}
