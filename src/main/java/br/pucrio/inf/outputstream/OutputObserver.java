package br.pucrio.inf.outputstream;

import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.Rule;

/**
 * Observer interface for output subscribers.
 * 
 * Interested listeners need to implement this interface to be alerted when the
 * engine filters new messages.
 */
public interface OutputObserver {

	/**
	 * A method which will be triggered by the system output to indicate that
	 * it filtered a {@link Mensagem message} from a given {@link Rule rule}.
	 * 
	 * @param rule the CEP rule.
	 * @param msg  the filtered message.
	 */
	public void update(Rule rule, Mensagem msg);
	
	/**
	 * This method will be called when to indicate that the system destroyed a
	 * given {@link Rule rule}.
	 * 
	 * @param rule the CEP rule.
	 */
	public void destroy(Rule rule);
}
