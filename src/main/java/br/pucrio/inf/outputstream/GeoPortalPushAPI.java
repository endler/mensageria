package br.pucrio.inf.outputstream;

import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.MensagemFiltrada;
import br.pucrio.inf.domain.Rule;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.security.auth.login.CredentialException;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.log4j.Logger;

/**
 * This class provides operations for pushing the filtered messages to the
 * GeoPortal API.
 *
 * To do so, this class follows the Observer pattern and subscribes to filtered
 * messages of given rules. When the engine filters a message it will trigger
 * the {@link #update(Rule, Mensagem)} method to dispatch the detected message.
 * This method contains both, the filtered message and the rule that detected
 * it.
 */
public class GeoPortalPushAPI {

    /**
     * LOGGER.
     */
    private static Logger LOGGER = Logger.getLogger(GeoPortalPushAPI.class.getCanonicalName());

    /**
     * GeoPortal key (chave)
     */
    private String key;

    // TODO: Rever isso aqui.
    /**
     * GeoPortal source ID
     */
    private String sourceID = "1";

    /**
     * HTTP Client (to consume the REST API)
     */
    private OkHttpClient httpClient;

    /**
     * JSON Mapper to deserialize the HTTP calls
     */
    private ObjectMapper jsonMapper;

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param credentials the API key (property 'chave').
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public GeoPortalPushAPI(Properties credentials) throws CredentialException, IOException {
        this((String) credentials.get("chave"));
    }

    /**
     * GeoPortal API constructor, which provides methods to consume the
     * GeoPortal HTTP services.
     *
     * @param key a String representing the API key.
     * @throws CredentialException if properties contains wrong or no correct
     * 'chave' property
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public GeoPortalPushAPI(String key) throws CredentialException, IOException {
        this.key = key;
        this.httpClient = new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true).build();
        this.jsonMapper = new ObjectMapper();

        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        checkKeyValidity(key);
    }

    /**
     * This method verify if the provided API key is valid.
     *
     * @param key the api key (chave)
     * @return true if the key is valid, false otherwise.
     *
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public boolean checkKeyValidity(String key) throws IOException {
        HttpUrl httpURL = baseURLBuilder().build();

        Request request = new Request.Builder().url(httpURL).build();
        Response response = httpClient.newCall(request).execute();

        String json = response.body().string();
        if ((json.toLowerCase().contains("invalida")) || (json.isEmpty()) || (json.equalsIgnoreCase(""))) {
            LOGGER.error("Incorrect or Malformated key to access the GeoPortal API");
            return false;
        } else {
            LOGGER.info("Successfully connected to the GeoPortal API");
            return true;
        }
    }

    /**
     * This method push the messages to the GeoPortal API
     *
     * @param msgs
     * @return a List of {@link Categoria}s
     * @throws IOException in the presence of a connection issue (e.g., if not
     * connected)
     */
    public boolean pushMessages(Collection<MensagemFiltrada> msgs) throws IOException {
        ArrayNode msgJSONArray = jsonMapper.createArrayNode();

        MensagemFiltrada.clearFilteredMessages();
        for (MensagemFiltrada filteredMSG : msgs) {
            Mensagem msg = filteredMSG.getMsg();
            String rule = filteredMSG.getFilter();
            filteredMSG.saveFiltered();

            ObjectNode json = jsonMapper.createObjectNode();
            json.put("id", msg.getId());
            json.put("category", msg.getCategoryID());
            json.put("latitude", msg.getLatitude());
            json.put("longitude", msg.getLongitude());

            // Gambiarra to get current Date
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
            String strDate = sdfDate.format(new Date());
            json.put("date", strDate);

            json.put("description", msg.getDescription());
            json.put("severity", filteredMSG.getSeverity());
            json.put("title", msg.getTitle());
            json.put("ruleName", rule);
            msgJSONArray.add(json);
        }

        String msgJSON = msgJSONArray.toString();

        RequestBody formBody = new FormBody.Builder()
                .add("chave", key)
                .add("json", msgJSON)
                .build();

        Request request = new Request.Builder()
                .url("http://geoportal.cor.rio.gov.br/webservices/_recebe.cfm")
                .post(formBody)
                .build();

        long before = System.currentTimeMillis();
        Response response = httpClient.newCall(request).execute();
        long after = System.currentTimeMillis();
        LOGGER.info("HTTP DELAY FOR PUSHING MESSAGES IS: " + (after - before) + " ms");

        String responseSTR = response.body().string();
        if ((responseSTR.toLowerCase().contains("ok")) || (responseSTR.isEmpty()) || (responseSTR.equalsIgnoreCase(""))) {
            LOGGER.info("Successfully pushed the data to GeoPortal API");
            return true;
        } else {
            LOGGER.error("Incorrect or Malformated key/json to the GeoPortal API");
            return false;
        }
    }

    /**
     * A utility method for building the GeoPortal URL call.
     *
     * @return a http url builder
     */
    private HttpUrl.Builder baseURLBuilder() {
        HttpUrl.Builder httpURLBuilder = new HttpUrl.Builder()
                .scheme("http")
                .host("geoportal.cor.rio.gov.br")
                .addPathSegments("webservices/_recebe.cfm")
                .addQueryParameter("chave", key);
        return httpURLBuilder;
    }

}
