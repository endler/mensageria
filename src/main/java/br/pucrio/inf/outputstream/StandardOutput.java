package br.pucrio.inf.outputstream;

import java.util.Deque;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.log4j.Logger;

import br.pucrio.inf.domain.GeoPoI;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.domain.MensagemFiltrada;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import com.espertech.esper.event.map.MapEventBean;

public class StandardOutput implements UpdateListener {
    
    /** LOGGER. */
    private static Logger LOGGER = Logger.getLogger(StandardOutput.class.getCanonicalName());

    /** Singleton Instance. */
    private static StandardOutput singleton;

    /** Output Queues. */
    protected Deque<MensagemFiltrada> msgOutputQueue;
    
    //////////////////////////////////////////////////////////////////////////
    // Singleton Pattern
    //////////////////////////////////////////////////////////////////////////
    private StandardOutput() {
        msgOutputQueue = new LinkedBlockingDeque<>();
    }
    
    public static StandardOutput getInstance() {
        if (singleton == null) {
            singleton = new StandardOutput();
        }

        return singleton;
    }
      
    //////////////////////////////////////////////////////////////////////////
    // Queue Methods
    //////////////////////////////////////////////////////////////////////////
    public void clear() {
        msgOutputQueue.clear();
    }
    
    public Deque<MensagemFiltrada> getMsgOutputQueue() {
        return msgOutputQueue;
    }
    
    //////////////////////////////////////////////////////////////////////////
    // CEP Listener
    //////////////////////////////////////////////////////////////////////////
    @Override
    public void update(EventBean[] newEvents, EventBean[] oldEvents) {
        for (EventBean eb : newEvents) {
            MensagemFiltrada filteredMSG = null;
            String filterName = (String) eb.get("regra");
            Mensagem mensagem = (Mensagem) eb.get("mensagem");

            Map<String, Object> mapKeys = ((MapEventBean) eb).getProperties();
            if (mapKeys.containsKey("criticidade") && mapKeys.containsKey("poi")) {
                int severity = (int) eb.get("criticidade");
                GeoPoI poi   = (GeoPoI) eb.get("poi");
                
                filteredMSG = new MensagemFiltrada(filterName, mensagem, severity, poi);
            } else if (mapKeys.containsKey("criticidade")) {
                int severity = (int) mapKeys.get("criticidade");

                filteredMSG = new MensagemFiltrada(filterName, mensagem, severity, null);
            } else {
                filteredMSG = new MensagemFiltrada(filterName, mensagem);
            }
            
            msgOutputQueue.add(filteredMSG);
            LOGGER.trace("Listener Received " + filteredMSG);
        }
    }
}
