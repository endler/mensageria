/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf.core;

import br.pucrio.inf.domain.Configuration;
import br.pucrio.inf.domain.ReadCSV;
import br.pucrio.inf.threads.APICycleThread;
import br.pucrio.inf.threads.PoIReadersThread;
import br.pucrio.inf.threads.RulesReaderThread;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.security.auth.login.CredentialException;

/**
 *
 * @author Rafael
 */
public class SilentMain {

    private final Properties config;
    private final ScheduledThreadPoolExecutor threadPool;

    public SilentMain() throws FileNotFoundException, IOException {
        this.config = Configuration.getProperties();
        this.threadPool = new ScheduledThreadPoolExecutor(2);
    }

    public static void main(String[] args) throws Exception {
        SilentMain main = new SilentMain();
        main.processingLayer();
    }

    private void processingLayer() throws CredentialException, IOException {
        this.threadPool.execute(new PoIReadersThread());
        this.threadPool.scheduleWithFixedDelay(new APICycleThread(config), 0, Long.valueOf(config.getProperty("delayAPIThread")), TimeUnit.SECONDS);
        this.threadPool.scheduleWithFixedDelay(new RulesReaderThread(), 0, Long.valueOf(config.getProperty("delayRuleThread")), TimeUnit.SECONDS);
    }

    private void readdata() {
        try {
            String line = "";
            PrintWriter writer = new PrintWriter("dataset/output.csv");

            ReadCSV reader = new ReadCSV();
            ArrayList<String> result = reader.readCSV("dataset/sentiment140test.csv");
            for (String twitt : result) {
                writer.println(twitt);
            }
            int i = 0;
            while (i++ < 900000) {
                writer.println(result.get(i));
            }
            writer.close();
        } catch (FileNotFoundException ex) {
            System.out.println("error");
        }
    }

}
