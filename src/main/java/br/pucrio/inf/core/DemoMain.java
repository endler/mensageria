/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pucrio.inf.core;

import br.pucrio.inf.domain.Configuration;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.security.auth.login.CredentialException;
import javax.swing.UIManager;

import br.pucrio.inf.gui.DemoGUI;
import br.pucrio.inf.threads.APICycleThread;
import br.pucrio.inf.threads.PoIReadersThread;
import br.pucrio.inf.threads.RulesReaderThread;

/**
 *
 * @author Rafael
 */
public class DemoMain {

    private final Properties config;
    private final ScheduledThreadPoolExecutor threadPool;
    private final DemoGUI demoGUI;

    public DemoMain() throws FileNotFoundException, IOException {
        this.config = Configuration.getProperties();
        this.threadPool = new ScheduledThreadPoolExecutor(2);
        this.demoGUI = new DemoGUI();
        this.demoGUI.setVisible(true);
    }

    public static void main(String[] args) throws Exception {
        for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                UIManager.setLookAndFeel(info.getClassName());
                break;
            }
        }

        DemoMain demo = new DemoMain();
        demo.processingLayer();
    }

    private void processingLayer() throws CredentialException, IOException {
        this.threadPool.execute(new PoIReadersThread());
        this.threadPool.scheduleWithFixedDelay(new APICycleThread(config, this.demoGUI), 0, Long.valueOf(config.getProperty("delayAPIThread")), TimeUnit.SECONDS);
        this.threadPool.scheduleWithFixedDelay(new RulesReaderThread(this.demoGUI), 0, Long.valueOf(config.getProperty("delayRuleThread")), TimeUnit.SECONDS);
    }

}
