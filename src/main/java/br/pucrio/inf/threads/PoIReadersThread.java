package br.pucrio.inf.threads;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.referencing.operation.TransformException;

import br.pucrio.inf.cep.CEPEngine;
import br.pucrio.inf.database.ConnectionSGBD;
import br.pucrio.inf.domain.GeoPoI;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

public class PoIReadersThread implements Runnable {

    private final ConnectionSGBD connection;
    private final CEPEngine cep;
    private GeometryFactory geometryFactory;

    public PoIReadersThread() {
        this.connection = ConnectionSGBD.getInstance();
        this.cep = CEPEngine.getInstance();
        this.geometryFactory = JTSFactoryFinder.getGeometryFactory();
    }

//    private void playground() throws TransformException {
//        Coordinate[] c = new Coordinate[5];
//        c[0] = new Coordinate(-49.242986,-16.662430);
//        c[1] = new Coordinate(-49.241999,-16.664465);
//        c[2] = new Coordinate(-49.239146,-16.663828);
//        c[3] = new Coordinate(-49.239832,-16.661443);
//        c[4] = new Coordinate(-49.242986,-16.662430);
//
//        Geometry geo = geometryFactory.createPolygon(c);
//        
//        Point p = geometryFactory.createPoint(new Coordinate(-49.246870, -16.665493));
//        
//        double d = geo.distance(p);
//        d = (d * (Math.PI / 180) * 6378137);
//
//        System.out.println("Distancia: " + geo.distance(p));
//        System.out.println("Distancia: " + d);
//        
//        // Debug
//        System.out.println("debug");
//        Point px = geometryFactory.createPoint(new Coordinate(-49.23928499221802,-16.663945187242863));
//        Point py = geometryFactory.createPoint(new Coordinate(-49.302263259887695,-16.726467886678584));
//        
//        double pd = px.distance(py);
//        pd = (pd * (Math.PI / 180) * 6378137);
//        
//        System.out.println("Distancia: " + px.distance(py));
//        System.out.println("Distancia: " + pd);
//        System.out.println("distance: " + JTS.orthodromicDistance(px.getCoordinate(), py.getCoordinate(), DefaultGeographicCRS.WGS84));
//       
//    }

    @Override
    public void run() {
        try {
            PreparedStatement pps = connection.getConnection().prepareStatement("select * from v_poi");
            ResultSet rs = pps.executeQuery();
            
            String regionName = "";
            LinkedList<Coordinate> coordinates = new LinkedList<Coordinate>();
            while (rs.next()) {
                if (!rs.getString(1).equalsIgnoreCase(regionName) && !regionName.equalsIgnoreCase("")) {
                    Geometry geo = geometryFactory.createPolygon((Coordinate[]) coordinates.toArray(new Coordinate[coordinates.size()]));
                    GeoPoI poi = new GeoPoI(regionName, geo);

                    // CEP
                    cep.getCepEngine().getEPRuntime().sendEvent(poi);
                    
                    // Cleaning
                    coordinates = new LinkedList<Coordinate>();
                }
        
                regionName = rs.getString(1);
                coordinates.add(new Coordinate(rs.getDouble(3), rs.getDouble(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PoIReadersThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
