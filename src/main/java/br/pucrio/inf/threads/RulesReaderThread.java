package br.pucrio.inf.threads;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.pucrio.inf.cep.CEPEngine;
import br.pucrio.inf.database.ConnectionSGBD;
import br.pucrio.inf.domain.Rule;
import br.pucrio.inf.domain.SituacaoRegra;
import br.pucrio.inf.gui.DemoGUI;

public class RulesReaderThread implements Runnable {

    private final ConnectionSGBD connection;
    private final CEPEngine cep;
    private DemoGUI demoGUI;


    public RulesReaderThread() {
        this(null);
    }

    public RulesReaderThread(DemoGUI demoGUI) {
        this.connection = ConnectionSGBD.getInstance();
        this.cep = CEPEngine.getInstance();
        this.demoGUI = demoGUI;
    }

    @Override
    public void run() {
        try {
            PreparedStatement pps = connection.getConnection().prepareStatement("select * from regras order by id_regra");
            ResultSet rs = pps.executeQuery();
            Set<Rule> rules = new HashSet<>();
            while (rs.next()) {
                SituacaoRegra situacao = SituacaoRegra.valueOf(rs.getString(4));
                
                if (situacao == SituacaoRegra.ativa) {
                    rules.add(new Rule(rs.getInt(1), rs.getString(2), rs.getString(3), SituacaoRegra.valueOf(rs.getString(4))));
                }
            }
            cep.synchronizeRuleSet(rules);
            
            if (demoGUI != null) {
                demoGUI.addInputFilters(rules);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RulesReaderThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
