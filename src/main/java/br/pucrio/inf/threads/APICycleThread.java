package br.pucrio.inf.threads;

import br.pucrio.inf.cep.CEPEngine;
import br.pucrio.inf.domain.Categoria;
import br.pucrio.inf.domain.Mensagem;
import br.pucrio.inf.gui.DemoGUI;
import br.pucrio.inf.inputstream.PerformanceAPI;
import br.pucrio.inf.outputstream.PerformancelPushAPI;
import br.pucrio.inf.outputstream.StandardOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.security.auth.login.CredentialException;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.log4j.Logger;

public class APICycleThread implements Runnable {

    /**
     * LOGGER.
     */
    private static Logger LOGGER = Logger.getLogger(APICycleThread.class.getCanonicalName());

    private PerformanceAPI api;
    private PerformancelPushAPI outAPI;

    private CEPEngine cep;
    private DemoGUI demoGUI;

    private ExecutorService cycleThreadPool;

    public APICycleThread(Properties properties) {
        this(properties, null);
    }

    public APICycleThread(Properties properties, DemoGUI demoGUI) {
        this.cep = CEPEngine.getInstance();
        this.demoGUI = demoGUI;
        try {
            api = new PerformanceAPI(properties.getProperty("geoPortalKey", "992d45bf6e092372c38530963f0f8f37"));
            outAPI = new PerformancelPushAPI(properties.getProperty("pushGeoPortalKey", "5f99819ea62a8146aeeaf15886ec3761"));

            cycleThreadPool = Executors.newFixedThreadPool(16, new BasicThreadFactory.Builder().namingPattern("CycleThreadPool-%d").build());
        } catch (CredentialException | IOException ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    @Override
    public void run() {
        this.readAllToInput();
    }

    private void readAllToInput() {
        try {
            Long initialTime = System.currentTimeMillis();

            cep.clear();
            List<Categoria> categorias = api.listCategories();
            List<Mensagem> todasMensagens = new ArrayList<>();

            List<Future<List<Mensagem>>> categoryAnalysis = new ArrayList<Future<List<Mensagem>>>();

            for (Categoria listedCategory : categorias) {
                ProcessCategoryTask task = new ProcessCategoryTask(listedCategory);
                categoryAnalysis.add(cycleThreadPool.submit(task));
            }

            for (Future<List<Mensagem>> categoryTask : categoryAnalysis) {
                todasMensagens.addAll(categoryTask.get());
            }

            Long processTime = System.currentTimeMillis();

            if (demoGUI != null) {
                demoGUI.addInputMessages(todasMensagens);
                demoGUI.addOutputMessages(StandardOutput.getInstance().getMsgOutputQueue());
            }
            outAPI.pushMessages(StandardOutput.getInstance().getMsgOutputQueue());

            Long totalTime = System.currentTimeMillis();

            LOGGER.info("PROCESS TIME: " + (processTime - initialTime) / 1000 + " seconds");
            LOGGER.info("COMPLETE TIME: " + (totalTime - initialTime) / 1000 + " seconds");
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    private class ProcessCategoryTask implements Callable<List<Mensagem>> {

        private Categoria category;

        public ProcessCategoryTask(Categoria category) {
            this.category = category;
        }

        @Override
        public List<Mensagem> call() throws Exception {
            category.save();
            List<Mensagem> mensagens = api.getMessages(category);
            for (Mensagem msg : mensagens) {
                msg.save();
                cep.getCepEngine().getEPRuntime().sendEvent(msg);
            }

            return mensagens;
        }

    }
}
