--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.0
-- Dumped by pg_dump version 9.5.0

-- Started on 2016-05-11 10:07:57

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2124 (class 0 OID 426253)
-- Dependencies: 186
-- Data for Name: regras; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO regras VALUES (14, 'select ''Filtro Rebouças'' as regra, * from MensagemWindow where description like ''%REBOUÇAS%''', 'Palavras chave', 'ativa');
INSERT INTO regras VALUES (15, 'select ''Linha vermelha'' as regra, * from MensagemWindow where description like ''%VERMELHA%''', 'Linha vermelha', 'ativa');
INSERT INTO regras VALUES (12, 'SELECT ''Ocorrências de Trânsito'' AS regra, * FROM MensagemWindow WHERE categoryID  > 1002', 'Teste 3', 'ativa');
INSERT INTO regras VALUES (13, 'SELECT ''Ocorrências de Trânsito'' AS regra, * FROM MensagemWindow WHERE categoryID  = 751', 'Manifestações', 'inativa');


--
-- TOC entry 2130 (class 0 OID 0)
-- Dependencies: 187
-- Name: regras_id_regra_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('regras_id_regra_seq', 15, true);


-- Completed on 2016-05-11 10:07:57

--
-- PostgreSQL database dump complete
--

