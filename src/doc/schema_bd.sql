-- SCHEMA MENSAGERIA --
create table categorias (
 id_categoria serial not null,
 descricao text not null
);
alter table categorias add constraint pk_categorias primary key (id_categoria);


create table fonte (
 id_fonte varchar(2048) not null,
 descricao varchar(2048) not null
);
alter table fonte add constraint pk_fonte primary key (id_fonte);


create table mensagens (
  id_mensagem integer NOT NULL,
  descricao varchar(2048) NOT NULL,
  criticidade varchar(50) NOT NULL,
  "timestamp" timestamp without time zone NOT NULL,
  id_fonte varchar(2048) NOT NULL,
  icone varchar(2048),
  id_categoria integer NOT NULL,
  situacao varchar(50) NOT NULL,
  json text NOT NULL,
  titulo varchar(1024) NOT NULL
);
alter table mensagens add constraint pk_mensagens primary key (id_mensagem);


create table regras (
 id_regra serial not null,
 regra text not null,
 descricao text
);

alter table regras add constraint pk_regras primary key (id_regra);


create table geometrias (
  coord_x integer NOT NULL,
  coord_y integer NOT NULL,
  id_mensagem integer NOT NULL,
  tipo varchar(60) NOT NULL
);
alter table geometrias add constraint pk_geometrias primary key (coord_x,coord_y,id_mensagem);


alter table mensagens add constraint fk_mensagens_0 foreign key (id_fonte) references fonte (id_fonte);
alter table mensagens add constraint fk_mensagens_1 foreign key (id_categoria) references categorias (id_categoria);
alter table geometrias add constraint fk_geometrias_0 foreign key (id_mensagem) references mensagens (id_mensagem);


-- TRIGGERS --

INSERT INTO public.fonte(id_fonte, descricao) VALUES (1, 'geoportal');


CREATE OR REPLACE FUNCTION public.insert_categoria(
    id integer,
    desccategoria text,
    id_categoria text)
  RETURNS void AS
$BODY$ 
DECLARE 
BEGIN 
	INSERT INTO categorias values (id, descCategoria, id_categoria); 
	EXCEPTION
	    WHEN unique_violation
	    THEN NULL;
END; 
$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION public.insert_mensagem(
    id_mensagem integer,
    titulo text,
    descricao text,
    criticidade text,
    id_fonte integer,
    icone text,
    id_categoria text,
    situacao text, 
	json text)
  RETURNS void AS
$BODY$ 
DECLARE 
BEGIN 
	INSERT INTO public.mensagens VALUES (id_mensagem, descricao, criticidade, now(), id_fonte, icone, id_categoria, situacao, json, titulo, false);
	EXCEPTION
	    WHEN unique_violation
	    THEN NULL;
END; 
$BODY$
  LANGUAGE plpgsql;
  

CREATE FUNCTION public.insert_geometria(
    id_mensagem integer,
    latitude float,
    longitude float,
    tipo text)
  RETURNS void AS
$BODY$ 
DECLARE 
BEGIN 
	INSERT INTO public.geometrias VALUES (latitude, longitude, id_mensagem, tipo);
	EXCEPTION
	    WHEN unique_violation
	    THEN NULL;
END; 
$BODY$
  LANGUAGE plpgsql;

  
  
  
CREATE OR REPLACE FUNCTION public.mensagens_para_analise()
  RETURNS TABLE(s_id_mensagem integer, s_descricao character varying, s_criticidade character varying, s_id_fonte character varying, s_icone character varying, s_id_categoria text, s_situacao character varying, s_json text, s_titulo character varying) AS
$BODY$
 DEClARE
BEGIN
    RETURN QUERY select id_mensagem, descricao, criticidade, id_fonte, icone, id_categoria, situacao, json, titulo from mensagens where analisada = false;
    IF FOUND THEN
        update mensagens set analisada = true;
    END IF;
    RETURN;
 END
$BODY$
  LANGUAGE plpgsql;

 
 
 
  -- SCRIPTS DE TESTE --
  
  
select insert_categoria(1, 'cat 1', '1');
select insert_mensagem(1, 'titulo 1', 'MSG 1', 'bad', 1, 'icone', 1, 'aberta', 'JSON');
select insert_geometria(1, 100, -100, 'coords');
select insert_geometria(1, 100, -200, 'coords');

select * from categorias;
select * from mensagens;
select * from geometrias;

delete from geometrias;
delete from mensagens;
delete from categorias;



select * from msg()
select * from mensagens
update mensagens set analisada = false where id_categoria = 1024
